#!/usr/bin/env python3
#
# This sets the gitlab permission levels for core contributors.

import gitlab
import os
from colorama import Fore, Style

private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
if not private_token:
    print(
        Fore.RED
        + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!'
        + Style.RESET_ALL
    )
    exit(1)


gl = gitlab.Gitlab('https://gitlab.com', api_version=4, private_token=private_token)
group = gl.groups.get('fdroid', lazy=True)
wiki = gl.projects.get('fdroid/wiki', lazy=True)

project_member_ids = set()
for member in wiki.members.list(all=True):
    project_member_ids.add(member.id)

report = [
    '%s\tDEVELOPER---------------------' % gitlab.DEVELOPER_ACCESS,
    '%s\tMASTER------------------------' % gitlab.MASTER_ACCESS,
    '%s\tOWNER-------------------------' % gitlab.OWNER_ACCESS,
]

for groupmember in group.members.list(all=True):
    access_level = None
    if groupmember.access_level > gitlab.DEVELOPER_ACCESS:
        report.append(
            '%s\t%s\t%s' % (groupmember.access_level, groupmember.id, groupmember.name)
        )
        continue
    elif groupmember.access_level == gitlab.DEVELOPER_ACCESS:
        access_level = gitlab.MASTER_ACCESS
    else:
        access_level = gitlab.DEVELOPER_ACCESS

    if groupmember.id in project_member_ids:
        member = wiki.members.get(groupmember.id)
    else:
        member = wiki.members.create(
            {'user_id': groupmember.id, 'access_level': gitlab.DEVELOPER_ACCESS}
        )
    member.access_level = access_level
    try:
        member.save()
    except gitlab.exceptions.GitlabUpdateError as e:
        print('Cannot change member access level:', e)

print('-----------------------------------------------------------------------')
print('Settings for https://gitlab.com/fdroid/wiki\n')
for member in wiki.members.list(all=True):
    report.append('%s\t%s\t%s' % (member.access_level, member.id, member.name))
print('\n'.join(sorted(report, reverse=True)))
